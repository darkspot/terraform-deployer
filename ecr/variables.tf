
variable "name" {
  description = "The name of the App ECR"
  type        = string
}

variable "aws_region" {
  description = "AWS region you want to use"
  type        = string
}