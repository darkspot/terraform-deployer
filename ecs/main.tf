resource "aws_ecs_cluster" "app" {
  name = var.name
  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

resource "aws_vpc" "app" {
  cidr_block       = "${local.vpc_cidr_ip_address_prefix}.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = var.name
  }
}

resource "aws_internet_gateway" "app" {
  vpc_id = aws_vpc.app.id
  tags = {
    Name = var.name
  }
  depends_on = [aws_vpc.app]
}

resource "aws_eip" "app" {
  vpc        = true
  depends_on = [aws_internet_gateway.app]
  tags = {
    Name = var.name
  }
}

resource "aws_security_group" "app-ecs" {
  name        = "${var.name}-ecs"
  description = "Security Group for ECS Task of app"
  vpc_id      = aws_vpc.app.id

  ingress {
    description = "Security Group Inbound HTTPS for ECS Task of app"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.name}-ecs"
  }
}

resource "aws_subnet" "primary" {
  vpc_id     = aws_vpc.app.id
  availability_zone = data.aws_availability_zones.available.names[0]
  cidr_block = "${local.vpc_cidr_ip_address_prefix}.0.0/24"

  tags = {
    Name = "${var.name}-primary"
  }
  depends_on = [aws_vpc.app]
}

resource "aws_subnet" "secondary" {
  vpc_id     = aws_vpc.app.id
  availability_zone = data.aws_availability_zones.available.names[1]
  cidr_block = "${local.vpc_cidr_ip_address_prefix}.0.0/24"

  tags = {
    Name = "${var.name}-secondary"
  }
  depends_on = [aws_vpc.app]
}

resource "aws_route_table" "app" {
  vpc_id = aws_vpc.app.id

  route {
    cidr_block = "${local.vpc_cidr_ip_address_prefix}.0.0/0"
    gateway_id = aws_internet_gateway.app.id
  }

  tags = {
    Name = "app Route Table"
  }
  depends_on = [aws_vpc.app, aws_internet_gateway.app]
}

resource "aws_route_table_association" "app" {
  subnet_id      = aws_subnet.primary.id
  route_table_id = aws_route_table.app.id
  depends_on     = [aws_subnet.primary, aws_route_table.app]

}

resource "aws_lb" "app" {
  name               = "${var.name}-app"
  load_balancer_type = "application"
  internal           = false

  subnet_mapping {
    subnet_id     = aws_subnet.primary.id
    allocation_id = aws_eip.app.id
  }

  subnet_mapping {
    subnet_id     = aws_subnet.secondary.id
    allocation_id = aws_eip.app.id
  }

  tags = {
    Environment = "production"
  }
  depends_on = [aws_subnet.primary, aws_subnet.secondary, aws_eip.app]
}

resource "aws_lb_target_group" "app" {
  name        = "${var.name}-target-group"
  port        = 80
  protocol    = "TCP"
  vpc_id      = aws_vpc.app.id
  target_type = "ip"
  lifecycle {
    create_before_destroy = true
  }
  stickiness {
    enabled = false
    type    = "lb_cookie"
  }
  depends_on = [aws_lb.app]
}

resource "aws_lb_listener" "app" {
  load_balancer_arn = aws_lb.app.arn
  port              = "80"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app.arn
  }
}

resource "aws_lb_listener" "https_app" {
  load_balancer_arn = aws_lb.app.arn
  port              = "80"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app.arn
  }
}

resource "aws_cloudwatch_log_group" "app" {
  name = "/ecs/${var.name}-app"
  tags = {
    Name = "${var.name}-app-log-group"
  }
}

resource "aws_cloudwatch_log_stream" "app" {
  name           = "app logs"
  log_group_name = aws_cloudwatch_log_group.app.name
}

resource "aws_ecs_task_definition" "app" {
  family                   = "service"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  task_role_arn            = aws_iam_role.ecs_task_execution_role.arn
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  cpu                      = 1024
  memory                   = 3072
  container_definitions = jsonencode([
    {
      "cpu"    = 1024
      "name"   = "app-definition"
      "image"  = "${var.app_ecr_repository_url}"
      "memory" = 3072
      "portMappings" = [
        {
          "containerPort" = 80
          "protocol"      = "tcp"
          "hostPort"      = 80
        }
      ],
      "logConfiguration" = {
        "logDriver" = "awslogs"
        "options" = {
          "awslogs-group"         = "/ecs/${var.name}-app"
          "awslogs-region"        = "${var.aws_region}"
          "awslogs-stream-prefix" = "ecs"
        }
      }
    }
  ])
}

resource "aws_ecs_service" "app-ecs-service" {
  name            = "${var.name}-service"
  cluster         = aws_ecs_cluster.app.id
  task_definition = aws_ecs_task_definition.app.arn
  desired_count   = 1
  launch_type     = "FARGATE"

  load_balancer {
    target_group_arn = aws_lb_target_group.app.arn
    container_name   = var.ecs_container_name
    container_port   = 80
  }

  network_configuration {
    subnets          = [aws_subnet.primary.id, aws_subnet.secondary.id]
    security_groups  = [aws_security_group.app-ecs.id]
    assign_public_ip = true
  }
  depends_on = [aws_lb.app, aws_lb_listener.app, aws_security_group.app-ecs]
}

data "aws_ecs_container_definition" "app" {
  task_definition = aws_ecs_task_definition.app.id
  container_name  = var.ecs_container_name
}