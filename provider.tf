/**********************************************************
 *
 * PROVIDER
 *
 *********************************************************/

# the default provider is for the account we are deploying into (uses assume role)
provider "aws" {
  profile = var.aws_profile
  region  = var.aws_region
  version = "~> 2.55.0"
}

# the named 'root' AWS provider is for our root account, where shared SFTP and S3 buckets are
provider "aws" {
  alias   = "root"
  profile = var.aws_profile
  region  = var.aws_region
  version = "~> 2.55.0"
}
