
variable "name" {
  description = "The name of the App ECR"
  type        = string
}

variable "app_ecr_repository_url" {
  description = "App ECR Repository URL"
  type        = string
}

variable "aws_region" {
  description = "AWS region you want to use"
  type        = string
}

variable "ecs_container_name" {
  description = "ECS Container name you want to use"
  type        = string
}

variable "az_count" {
  default = "2"
}