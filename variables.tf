
variable "name" {
  description = "The name of the app"
  type        = string

}

variable "aws_profile" {
  description = "AWS credentials profile you want to use"
  type        = string
}

variable "aws_region" {
  description = "AWS region you want to use"
  type        = string
  default     = "ap-southeast-1"
}

variable "deployment_account_id" {
  description = "The ID of the account to deploy to (will be used to assume the role in that account)"
  type        = string
}

variable "ecs_container_name" {
  description = "ECS Container name you want to use"
  type        = string
  default     = "${var.name}-container"
}