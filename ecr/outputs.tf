
output "app_ecr_repository_url" {
  description = "The ECR URL of the app"
  value       = aws_ecr_repository.app.repository_url
}