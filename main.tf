module "deployer-ecr" {
  source = "./ecr"
  name   = "${var.name}-ecr"
  aws_region = var.aws_region
}

module "deployer-ecs" {
  source                        = "./ecs"
  name                          = "${var.name}-ecs"
  app_ecr_repository_url        = module.deployer-ecr.app_ecr_repository_url
  aws_region                    = var.aws_region
}

module "ecs-fargate-service" {
  source                        = "cn-terraform/ecs-fargate-service/aws"
  version                       = "2.0.14"
  container_name                = module.deployer-ecr.app_ecr_repository_url
  # insert the 10 required variables here
}